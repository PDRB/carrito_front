import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { MatTableModule } from '@angular/material/table' 
import { NgModule } from  '@angular/core';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css'],
  
})

export class BuscadorComponent implements OnInit {
  dataSource  = [];
  displayedColumns: string[] = ['id', 'descripcion' , 'monto', 'cod_art'];

  constructor(private dataService: DataService) { }
  ngOnInit(): void {
    

    this.dataService.pedirProductos().subscribe((data: any[])=>{
      this.dataSource = data['data'] 
      console.log(this.dataSource  + "Aca esta la data")
      });
     
  }
  

}
