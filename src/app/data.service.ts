import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "http://localhost:4567/enviarProductos";
  
  constructor(private httpClient: HttpClient) { }

  public pedirProductos(){

    
    return this.httpClient.get(this.REST_API_SERVER);
  }




}
